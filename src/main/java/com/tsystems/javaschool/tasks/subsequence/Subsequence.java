package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public static boolean find(List x, List y) {

        if (x==null || y==null)
            throw new IllegalArgumentException();

        if (x.isEmpty())
            return true;
        else if (y.isEmpty())
            return false;

        int k=0; //initial index of the sequence y
        boolean flag=false; //was found
        for(int i=0;i<x.size();i++) //loop through the sequence x
        {
            flag=false;
            for(int j=k;j<y.size();j++) //loop through the sequence y
            {
                if (x.get(i).equals(y.get(j))) //if equals
                {
                    k=j+1;
                    flag=true;
                    break;
                }
            }
            if (flag==false) //if wasn't found
                return false;
        }
        return flag;
    }
}
