package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.LinkedList;
import java.util.Locale;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {

        if (statement=="")
            return null;

        if (statement==null)
            return null;

        LinkedList<Double> digits = new LinkedList<Double>();   //array of digits
        LinkedList<Character> operators = new  LinkedList<Character>(); //array of operators
        int par=0; // counter of opening parentheses
        boolean flag=false; //alternation of digits and signs

        for (int i = 0; i < statement.length(); i++) //loop for reading a statement
        {
            char c = statement.charAt(i);
            if (c==' ') //if space
                continue;
            if (c == '(') //if opening parenthesis
            {
                operators.add('(');
                par++;
            }
            else if (c == ')') //if closing parenthesis
            {
                if (par!=0) //if there were opening parentheses
                {
                    while (operators.getLast() != '(')
                        if (!operation(digits, operators.removeLast())) //execution of an operation
                            return null;
                    operators.removeLast();
                    par--;
                }
                else return null;
            }
            else if (isOperator(c)) //if operator
            {
                if (!flag) //if two operators successively, then an error
                    return null;
                else flag=false;
                while (!operators.isEmpty() && priority(operators.getLast()) >= priority(c)) //comparison of priorities
                    if (!operation(digits, operators.removeLast())) //execution of an operation
                        return null;
                operators.add(c);
            }
            else if (Character.isDigit(statement.charAt(i))) //if digit
            {
                if (flag) //if two numbers successively, then an error
                    return null;
                else flag=true;
                String dig = ""; //string for digits
                while (i < statement.length() && (Character.isDigit(statement.charAt(i)) || ((statement.charAt(i) == '.'))))
                {                                                                       //while digit or dot
                    if (statement.charAt(i) == '.') //if dot, then check the presence before it a number
                        if (!Character.isDigit(statement.charAt(i - 1)))
                            return null;
                    dig += statement.charAt(i++); //add to number
                }
                --i;
                digits.add(Double.parseDouble(dig));
            }
            else return null; //if incorrect character was entered, then an error
        }

        if (!flag) //if there is an operator at the end, then an error
            return null;

        if (par!=0) //if the number of parentheses is incorrect, then an error
            return null;

        while (!operators.isEmpty()) //выполняем все операции
            if (!operation(digits, operators.removeLast()))
                return null;

        Locale locale  = new Locale("en", "UK"); //output the result in the required format
        String pattern = "###.#######";
        DecimalFormat decimalFormat = (DecimalFormat)NumberFormat.getNumberInstance(locale);
        decimalFormat.applyPattern(pattern);
        return decimalFormat.format(digits.get(0));
    }

    public static boolean isOperator(char c) //is the character an operator
    {
        return c == '+' || c == '-' || c == '*' || c == '/';
    }

    public boolean operation(LinkedList<Double> digits, char operator) //execution of an operation
    {
        double dig1 = digits.removeLast();
        double dig2 = digits.removeLast();
        switch (operator)
        {
            case '+':
                digits.add(dig2 + dig1);
                break;
            case '-':
                digits.add(dig2 - dig1);
                break;
            case '*':
                digits.add(dig2 * dig1);
                break;
            case '/': {
                if (dig1==0) //if division by 0, then an error
                    return false;
                else
                    digits.add(dig2 / dig1);
                break;
            }
        }
        return true;
    }

    public static int priority(char operator) // priority of operations
    {
        switch (operator) {
            case '+':
            case '-':
                return 1;
            case '*':
            case '/':
                return 2;
            default:
                return -1;
        }
    }

}
