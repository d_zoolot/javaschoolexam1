package com.tsystems.javaschool.tasks.pyramid;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        int[][] matr;   //result matrix
        int size = inputNumbers.size(); //size of input array

        if (inputNumbers.contains(null))
            throw new CannotBuildPyramidException();

        if (size>Integer.MAX_VALUE-2) //check for acceptable size
            throw new CannotBuildPyramidException();

        int cnt = 0;  //check the size
        int rows = 1;
        int cols = 1;
        while (cnt < size)
        {
            cnt = cnt + rows;
            rows++;
            cols = cols + 2;
        }

        if(size!=cnt)    //if the size of the array is correct for constructing the matrix
            throw new CannotBuildPyramidException();

        rows = rows - 1;    //number of rows in the matrix
        cols = cols - 2;    //number of cols in the matrix


        List<Integer> sortList = inputNumbers.stream().sorted().collect(Collectors.toList()); //sort

        matr = new int[rows][cols];
        for (int[] row : matr) //fill the matrix with zeros
        {
            Arrays.fill(row, 0);
        }

        int middle = (cols / 2); //middle of the row
        cnt = 1; //number of numbers in the row
        int index = 0; //index of the number in the array

        for (int i = 0, shift = 0; i < rows; i++, shift++, cnt++) //basic loop of filling the matrix with numbers
        {
            int start = middle - shift; //initial position for filling
            for (int j = 0; j < cnt * 2; j += 2, index++) //populating cells in a row
            {
                matr[i][start + j] = sortList.get(index);
            }
        }

        return matr;
    }
}


